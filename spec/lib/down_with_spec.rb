require 'spec_helper'
require_relative '../../lib/down_with'

class Coffee
  extend DownWith

  argument_must_be_integer 0
  down_with def pour(ounces)
    'ok'
  end

  argument_must_be_integer(1)
  return_must_be_string
  down_with def make(bean_type, ounces)
    bean_type == 'return an integer' ? 2 : 'works as expected'
  end
end

describe Coffee do
  specify 'pour accepts an integer' do
    expect{ subject.pour 2 }.to_not raise_error
  end

  specify 'pour rejects a string' do
    expect{ subject.pour '2' }.to raise_error
  end

  specify 'make accepts an integer in position 1' do
    expect{ subject.make 'colombian', 2 }.to_not raise_error
  end


  specify 'make rejects a string in position 1' do
    expect{ subject.make 'colombian', '2' }.to raise_error
  end

  specify 'make must return a string' do
    expect(subject.make 'colombian', 2).to eq 'works as expected'
  end

  specify 'make throws an error if it returns an integer' do
    expect{ subject.make 'return an integer', 2 }.to raise_error
  end
end
