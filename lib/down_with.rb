module DownWith
  def down_with_preconditions
    @down_with_preconditions ||= []
  end

  def down_with_postconditions
    @down_with_postconditions ||= []
  end

  def return_must_be_string method_symbol = nil
    down_with_postconditions << ->(ret) {
      raise "not a string" unless ret.is_a?(String)
    }

  end

  def argument_must_be_integer arg_idx, method_symbol = nil
    down_with_preconditions << ->(*args) {
      raise "arg at index <#{arg_idx}> is not an int" unless args[arg_idx].is_a?(Fixnum)
    }

  end

  def down_with method_symbol
    # finish chaining
    alias_method "#{method_symbol}_without_conditions", method_symbol

    pres = down_with_preconditions.dup
    posts = down_with_postconditions.dup
    define_method method_symbol do |*args|
      pres.each do |condition|
        condition.call(*args)
      end

      ret = send("#{method_symbol}_without_conditions", *args)

      posts.each do |condition|
        condition.call(ret)
      end

      ret
    end
    down_with_preconditions.clear
    down_with_postconditions.clear
  end
end
