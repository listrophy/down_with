# -*- encoding: utf-8 -*-
require File.expand_path('../lib/down_with/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ['Josh Davey', 'Bradley Grzesiak']
  gem.email         = ['joshua.davey@hashrocket.com', 'brad@bendyworks.com']
  gem.description   = %q{Aspect-Oriented Programming in Ruby}
  gem.summary       = %q{Aspect-Oriented Programming in Ruby, providing preconditions and postconditions for your methods}
  gem.homepage      = "https://gitlab.com/listrophy/down_with"

  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.name          = "down_with"
  gem.require_paths = ["lib"]
  gem.version       = DownWith::VERSION

  gem.add_development_dependency 'rspec'
end
